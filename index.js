var amqp = require('amqplib');

const queueName = 'some queue name';

const connectionString = 'some connection string'


const onConsume = (channel, message) => {

    if (message !== null) {

        const str = String.fromCharCode.apply(String, message.content);

        let messageAsJson = JSON.stringify(str);

        console.log(`Message Received: ${messageAsJson}`);        
    }    

    channel.ack(message);
}


amqp.connect(connectionString).then(connection => {

    connection.createChannel().then(channel => {
        
        channel.consume(queueName, message => {

            onConsume(channel, message);

        });

    }).catch(error => {
        console.log(error);

        connection.close();
    })

}).catch(error => {
    console.log(error);
})
